#include "../../../squiggle.h"
#include "../../../squiggle_more.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double yearly_probability_nuclear_collapse(double year, uint64_t* seed)
{
    double successes = 0;
    double failures = (year - 1960);
    return sample_laplace(successes, failures, seed);
    // ^ can change to (successes + 1)/(trials + 2)
    // to get a probability,
    // rather than sampling from a distribution over probabilities.
}
double yearly_probability_nuclear_collapse_2023(uint64_t* seed)
{
    return yearly_probability_nuclear_collapse(2023, seed);
}

double yearly_probability_nuclear_collapse_after_recovery(double year, double rebuilding_period_length_years, uint64_t* seed)
{
    // assumption: nuclear
    double successes = 1.0;
    double failures = (year - rebuilding_period_length_years - 1960 - 1);
    return sample_laplace(successes, failures, seed);
}
double yearly_probability_nuclear_collapse_after_recovery_example(uint64_t* seed)
{
    double year = 2070;
    double rebuilding_period_length_years = 30;
    // So, there was a nuclear collapse in 2040,
    // then a recovery period of 30 years
    // and it's now 2070
    return yearly_probability_nuclear_collapse_after_recovery(year, rebuilding_period_length_years, seed);
}

double yearly_probability_nuclear_collapse_after_recovery_antiinductive(uint64_t* seed)
{
    return yearly_probability_nuclear_collapse(2023, seed) / 2;
}

int main()
{
    // set randomness seed
    uint64_t* seed = malloc(sizeof(uint64_t));
    *seed = 1000; // xorshift can't start with 0

    int n_samples = 1000000;

    // Before a first nuclear collapse
    printf("## Before the first nuclear collapse\n");
    double* yearly_probability_nuclear_collapse_2023_samples = malloc(sizeof(double) * (size_t)n_samples);
    for (int i = 0; i < n_samples; i++) {
        yearly_probability_nuclear_collapse_2023_samples[i] = yearly_probability_nuclear_collapse_2023(seed);
    }
    ci ci_90_2023 = array_get_90_ci(yearly_probability_nuclear_collapse_2023_samples, n_samples);
    printf("90%% confidence interval: [%f, %f]\n", ci_90_2023.low, ci_90_2023.high);

    // After the first nuclear collapse
    printf("\n## After the first nuclear collapse\n");

    double* yearly_probability_nuclear_collapse_after_recovery_samples = malloc(sizeof(double) * (size_t)n_samples);
    for (int i = 0; i < n_samples; i++) {
        yearly_probability_nuclear_collapse_after_recovery_samples[i] = yearly_probability_nuclear_collapse_after_recovery_example(seed);
    }
    ci ci_90_2070 = array_get_90_ci(yearly_probability_nuclear_collapse_after_recovery_samples, 1000000);
    printf("90%% confidence interval: [%f, %f]\n", ci_90_2070.low, ci_90_2070.high);

    // After the first nuclear collapse (antiinductive)
    printf("\n## After the first nuclear collapse (antiinductive)\n");
    double* yearly_probability_nuclear_collapse_after_recovery_antiinductive_samples = malloc(sizeof(double) * (size_t)n_samples);
    for (int i = 0; i < n_samples; i++) {
        yearly_probability_nuclear_collapse_after_recovery_antiinductive_samples[i] = yearly_probability_nuclear_collapse_after_recovery_antiinductive(seed);
    }
    ci ci_90_antiinductive = array_get_90_ci(yearly_probability_nuclear_collapse_after_recovery_antiinductive_samples, 1000000);
    printf("90%% confidence interval: [%f, %f]\n", ci_90_antiinductive.low, ci_90_antiinductive.high);

    // free seeds
    free(yearly_probability_nuclear_collapse_2023_samples);
    free(yearly_probability_nuclear_collapse_after_recovery_samples);
    free(yearly_probability_nuclear_collapse_after_recovery_antiinductive_samples);
    free(seed);
}
