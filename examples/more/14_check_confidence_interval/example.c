#include "../../../squiggle.h"
#include "../../../squiggle_more.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    // set randomness seed
    uint64_t* seed = malloc(sizeof(uint64_t));
    *seed = 1000; // xorshift can't start with a seed of 0

    int n = 1000000;
    double* xs = malloc(sizeof(double) * (size_t)n);
    for (int i = 0; i < n; i++) {
        xs[i] = sample_to(10, 100, seed);
    }
    ci ci_90 = array_get_90_ci(xs, n);
    printf("Recovering confidence interval of sample_to(10, 100):\n  low: %f, high: %f\n", ci_90.low, ci_90.high);

    free(xs);
    free(seed);
}
