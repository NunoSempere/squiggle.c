#include "../../../squiggle.h"
#include "../../../squiggle_more.h"
#include <stdio.h>
#include <stdlib.h>

// Estimate functions
double sample_beta_3_2(uint64_t* seed)
{
    return sample_beta(3.0, 2.0, seed);
}

int main()
{
    // set randomness seed
    uint64_t* seed = malloc(sizeof(uint64_t));
    *seed = 1000; // xorshift can't start with 0

    int n_samples = 1 * MILLION;
    double* xs = malloc(sizeof(double) * (size_t)n_samples);
    for (int i = 0; i < n_samples; i++) {
        xs[i] = sample_beta_3_2(seed);
    }

    printf("\n# Stats\n");
    array_print_stats(xs, n_samples);
    printf("\n# Histogram\n");
    array_print_histogram(xs, n_samples, 23);

    free(seed);
}
