#include "../../../squiggle.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    // set randomness seed
    uint64_t* seed = malloc(sizeof(uint64_t));
    *seed = 1000; // xorshift can't start with 0

    int n = 1000 * 1000;
    double* gamma_array = malloc(sizeof(double) * (size_t)n);
    for (int i = 0; i < n; i++) {
        gamma_array[i] = sample_gamma(1.0, seed);
    }
    printf("gamma(1) summary statistics = mean: %f, std: %f\n", array_mean(gamma_array, n), array_std(gamma_array, n));
    printf("\n");

    double* beta_array = malloc(sizeof(double) * (size_t)n);
    for (int i = 0; i < n; i++) {
        beta_array[i] = sample_beta(1, 2.0, seed);
    }
    printf("beta(1,2) summary statistics: mean: %f, std: %f\n", array_mean(beta_array, n), array_std(beta_array, n));
    printf("\n");

    free(gamma_array);
    free(beta_array);
    free(seed);
}
