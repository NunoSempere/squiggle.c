#include "../../../squiggle.h"
#include <stdio.h>
#include <stdlib.h>

double sample_model(uint64_t* seed){

    double sample_0(uint64_t* seed) { UNUSED(seed); return 0; }
    // Using a gcc extension, you can define a function inside another function
    double sample_1(uint64_t* seed) { UNUSED(seed); return 1; }
    double sample_few(uint64_t* seed) { return sample_to(1, 3, seed); }
    double sample_many(uint64_t* seed) { return sample_to(2, 10, seed); }

    double p_a = 0.8;
    double p_b = 0.5;
    double p_c = p_a * p_b;

    int n_dists = 4;
    double weights[] = { 1 - p_c, p_c / 2, p_c / 4, p_c / 4 };
    double (*samplers[])(uint64_t*) = { sample_0, sample_1, sample_few, sample_many };
    double result = sample_mixture(samplers, weights, n_dists, seed);

    return result;
}

int main()
{
    // set randomness seed
    uint64_t* seed = malloc(sizeof(uint64_t));
    *seed = 1000; // xorshift can't start with 0

    int n_samples = 1000000;
    double* result_many = (double*)malloc((size_t)n_samples * sizeof(double));
    for (int i = 0; i < n_samples; i++) {
        result_many[i] = sample_model(seed);
    }

    printf("result_many: [");
    for (int i = 0; i < 100; i++) {
        printf("%.2f, ", result_many[i]);
    }
    printf("]\n");

    free(seed);
}
