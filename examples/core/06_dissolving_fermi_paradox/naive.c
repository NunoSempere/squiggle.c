#include "../../../squiggle.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define VERBOSE 0

double sample_loguniform(double a, double b, uint64_t* seed)
{
    return exp(sample_uniform(log(a), log(b), seed));
}

int main()
{
    // Replicate <https://arxiv.org/pdf/1806.02404.pdf>, and in particular the red line in page 11.
    // Could also be interesting to just produce and save many samples.

    // set randomness seed
    uint64_t* seed = malloc(sizeof(uint64_t));
    *seed = UINT64_MAX / 64; // xorshift can't start with a seed of 0

    // Do this naïvely, without worrying that much about numerical precision
    double sample_fermi_naive(uint64_t * seed)
    {
        double rate_of_star_formation = sample_loguniform(1, 100, seed);
        double fraction_of_stars_with_planets = sample_loguniform(0.1, 1, seed);
        double number_of_habitable_planets_per_star_system = sample_loguniform(0.1, 1, seed);
        double rate_of_life_formation_in_habitable_planets = sample_lognormal(1, 50, seed);
        double fraction_of_habitable_planets_in_which_any_life_appears = -expm1(-rate_of_life_formation_in_habitable_planets);
        // double fraction_of_habitable_planets_in_which_any_life_appears = 1-exp(-rate_of_life_formation_in_habitable_planets);
        // but with more precision
        double fraction_of_planets_with_life_in_which_intelligent_life_appears = sample_loguniform(0.001, 1, seed);
        double fraction_of_intelligent_planets_which_are_detectable_as_such = sample_loguniform(0.01, 1, seed);
        double longevity_of_detectable_civilizations = sample_loguniform(100, 10000000000, seed);

        if(VERBOSE) printf(" rate_of_star_formation = %lf\n", rate_of_star_formation);
        if(VERBOSE) printf(" fraction_of_stars_with_planets = %lf\n", fraction_of_stars_with_planets);
        if(VERBOSE) printf(" number_of_habitable_planets_per_star_system = %lf\n", number_of_habitable_planets_per_star_system);
        if(VERBOSE) printf(" rate_of_life_formation_in_habitable_planets = %.16lf\n", rate_of_life_formation_in_habitable_planets);
        if(VERBOSE) printf(" fraction_of_habitable_planets_in_which_any_life_appears = %lf\n", fraction_of_habitable_planets_in_which_any_life_appears);
        if(VERBOSE) printf(" fraction_of_planets_with_life_in_which_intelligent_life_appears = %lf\n", fraction_of_planets_with_life_in_which_intelligent_life_appears);
        if(VERBOSE) printf(" fraction_of_intelligent_planets_which_are_detectable_as_such = %lf\n", fraction_of_intelligent_planets_which_are_detectable_as_such);
        if(VERBOSE) printf(" longevity_of_detectable_civilizations = %lf\n", longevity_of_detectable_civilizations);

        // Expected number of civilizations in the Milky way;
        // see footnote 3 (p. 5)
        double n = rate_of_star_formation * fraction_of_stars_with_planets * number_of_habitable_planets_per_star_system * fraction_of_habitable_planets_in_which_any_life_appears * fraction_of_planets_with_life_in_which_intelligent_life_appears * fraction_of_intelligent_planets_which_are_detectable_as_such * longevity_of_detectable_civilizations;

        return n;
    }

    double sample_are_we_alone_naive(uint64_t * seed)
    {
        double n = sample_fermi_naive(seed);
        return ((n > 1) ? 1 : 0);
    }

    double n = 1000000;
    double naive_fermi_proportion = 0;
    for (int i = 0; i < n; i++) {
        double result = sample_are_we_alone_naive(seed);
        if(VERBOSE) printf("result: %lf\n", result);
        naive_fermi_proportion += result;
    }
    printf("Naïve %% that we are not alone: %lf\n", naive_fermi_proportion / n);

    free(seed);

    /* 
        double invert(double x){
            return log(1-exp(-exp(-x)));
        }
        for(int i=0; i<64; i++){
            double j = i;
            printf("for %lf, log(1-exp(-exp(-x))) is calculated as... %lf\n", j, invert(j));
        }
    */
}
