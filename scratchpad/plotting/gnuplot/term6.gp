reset

# stats '-' nooutput
# show variables all
# max=STATS_max 
# min=STATS_min 
min=-1
max=25

n=1000 
width=(max-min)/n 
hist(x,width)=width*floor(x/width)+width/2.0

set xrange [min:max]
set yrange [0:45000]

unset xtics
unset ytics
unset border
set xtics out nomirror norotate offset 0,0
set ytics out nomirror norotate
set ytics in nomirror norotate offset 0,0
set tics scale 0,0,0,0

set xlabel "x"

set terminal dumb size 100, 25
plot '<cat' u (hist(0,width)):(1.0) smooth freq w boxes notitle


