MAKEFLAGS += --no-print-directory

## Formatter
STYLE_BLUEPRINT=webkit
FORMATTER=clang-format -i -style=$(STYLE_BLUEPRINT)

## Time to botec
TTB=./examples/more/12_time_to_botec_parallel/example

build-examples:
	cd examples/core && make all
	cd examples/more && make all

format-examples:
	cd examples/core && make format-all
	cd examples/more && make format-all

format: squiggle.c squiggle.h
	$(FORMATTER) squiggle.c squiggle.h
	$(FORMATTER) squiggle_more.c squiggle_more.h

lint:
	clang-tidy squiggle.c -- -lm
	clang-tidy squiggle_more.c -- -lm

profile:
	sudo perf record -g ./examples/more/12_time_to_botec_parallel/example 
	sudo perf report
	rm perf.data
	sudo perf stat ./examples/more/12_time_to_botec_parallel/example

time-linux: 
	gcc -O3 -Wall -Wextra -Wdouble-promotion -Wconversion examples/more/12_time_to_botec_parallel/example.c squiggle.c squiggle_more.c -lm -fopenmp -o examples/more/12_time_to_botec_parallel/example
	@echo "Running 100x and taking avg time: $(TTB)"
	@t=$$(/usr/bin/time -f "%e" -p bash -c 'for i in {1..100}; do OMP_PROC_BIND=TRUE $(TTB); done' 2>&1 >/dev/null | grep real | awk '{print $$2}' ); echo "scale=2; 1000 * $$t / 100" | bc | sed "s|^|Time using 16 threads: |" | sed 's|$$|ms|' && echo
